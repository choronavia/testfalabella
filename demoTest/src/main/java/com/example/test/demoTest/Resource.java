package com.example.test.demoTest;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class Resource {

    public Integer binaryToDecimal(String binario){
        int elevar = 0;
        Double decimal;
        int result = 0;

        if(null!=binario) {
            decimal = Double.valueOf(0);
            ArrayList list = new ArrayList();
            String[] arr = binario.split("");
            int base = 2;
            for(String a:arr) {
                System.out.println("Parametro: " + a);
                decimal = Math.pow(base,elevar);
                System.out.println("Decimal: " + decimal);
                result += Integer.parseInt(a) * decimal;
                System.out.println("Result: " + result);
                elevar++;
            }
        }
        else{
            result = 0;
        }


        return result;
    }
}
